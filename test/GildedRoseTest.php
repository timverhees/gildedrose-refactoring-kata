<?php

namespace App;

require './vendor/autoload.php';

class GildedRoseTest extends \PHPUnit\Framework\TestCase
{

  public function test_objects_array() {
    $items = (new GildedRose([
      new AgedBrie(10, 10),
    ]))->updateQuality();

    $this->assertEquals(9, $items[0]->sell_in);
    $this->assertEquals(11, $items[0]->quality);
  }

  public function test_aged_brie_before_sell_in_date_updates_normally()
  {
    $items = (new GildedRose([
      new AgedBrie(10, 10),
    ]))->updateQuality();

    $this->assertEquals(9, $items[0]->sell_in);
    $this->assertEquals(11, $items[0]->quality);
  }

  public function test_aged_brie_on_sell_in_date_updates_normally()
  {
    $items = (new GildedRose([
      new AgedBrie(1, 0)
    ]))->updateQuality();

    $this->assertEquals(0, $items[0]->sell_in);
    $this->assertEquals(1, $items[0]->quality);

  }

  public function test_aged_brie_after_sell_in_date_updates_normally()
  {
    $items = (new GildedRose([
      new AgedBrie(10, -5)
    ]))->updateQuality();

    $this->assertEquals(0, $items[0]->quality);
    $this->assertEquals( 9, $items[0]->sell_in);
  }

  public function test_aged_brie_before_sell_in_date_with_maximum_quality()
  {
    $items = (new GildedRose([
      new AgedBrie(4, 50)
    ]))->updateQuality();

    $this->assertEquals(50, $items[0]->quality);
    $this->assertEquals(3, $items[0]->sell_in);
  }

  public function test_aged_brie_on_sell_in_date_near_maximum_quality()
  {
    $items = (new GildedRose([
      new AgedBrie(0,49)
    ]))->updateQuality();

    $this->assertEquals(50, $items[0]->quality);
    $this->assertEquals( -1, $items[0]->sell_in);
  }

  public function test_aged_brie_on_sell_in_date_with_maximum_quality()
  {
    $items = (new GildedRose([
      new AgedBrie(50,0)
    ]))->updateQuality();

    $this->assertEquals(1, $items[0]->quality);
    $this->assertEquals( 49, $items[0]->sell_in);
  }

  public function test_aged_brie_after_sell_in_date_with_maximum_quality()
  {
    $items = (new GildedRose([
      new AgedBrie(-10,50)
    ]))->updateQuality();

    $this->assertEquals(50, $items[0]->quality);
    $this->assertEquals( -11, $items[0]->sell_in);
  }

  public function test_backstage_pass_before_sell_on_date_updates_normally()
  {
    $items = (new GildedRose([
      new BackstagePass(10, 10)
    ]))->updateQuality();

    $this->assertEquals($items[0]->quality, 12);
    $this->assertEquals($items[0]->sell_in, 9);
  }

  public function test_backstage_pass_more_than_ten_days_before_sell_on_date_updates_normally()
  {
    $items = (new GildedRose([
      new BackstagePass(15, 11)
    ]))->updateQuality();

    $this->assertEquals(12, $items[0]->quality);
    $this->assertEquals(14, $items[0]->sell_in);
  }

  public function test_backstage_pass_updates_by_three_with_five_days_left_to_sell()
  {
    $items = (new GildedRose([
      new BackstagePass(5, 5)
    ]))->updateQuality();

    $this->assertEquals(8, $items[0]->quality);
    $this->assertEquals(4, $items[0]->sell_in);
  }

  public function test_backstage_pass_drops_to_zero_after_sell_in_date()
  {
    $items = (new GildedRose([
      new BackstagePass(0, 10)
    ]))->updateQuality();

    $this->assertEquals(0, $items[0]->quality);
    $this->assertEquals(-1, $items[0]->sell_in);
  }

  public function test_backstage_pass_close_to_sell_in_date_with_max_quality()
  {
    $items = (new GildedRose([
      new BackstagePass( 6, 50)
    ]))->updateQuality();

    $this->assertEquals(50, $items[0]->quality);
    $this->assertEquals(5, $items[0]->sell_in);
  }

  public function test_backstage_pass_very_close_to_sell_in_date_with_max_quality()
  {
    $items = (new GildedRose([
      new BackstagePass( 3, 50)
    ]))->updateQuality();

    $this->assertEquals(50, $items[0]->quality);
    $this->assertEquals(2, $items[0]->sell_in);
  }

  public function test_backstage_pass_quality_zero_after_sell_date()
  {
    $items = (new GildedRose([
      new BackstagePass( -5, 0)
    ]))->updateQuality();

    $this->assertEquals($items[0]->quality, 0);
    $this->assertEquals($items[0]->sell_in, -6);
  }

  public function test_sulfuras_before_sell_in_date()
  {
    $items = (new GildedRose([
      new Sulfuras(10, 10)
    ]))->updateQuality();

    $this->assertEquals($items[0]->quality, 80);
    $this->assertEquals($items[0]->sell_in, 10);
  }

  public function test_sulfuras_on_sell_in_date()
  {
    $items = (new GildedRose([
      new Sulfuras(10, 0)
    ]))->updateQuality();

    $this->assertEquals(80, $items[0]->quality);
    $this->assertEquals(10, $items[0]->sell_in);
  }

  public function test_sulfuras_after_sell_in_date()
  {
    $items = (new GildedRose([
      new Sulfuras(-1, 10)
    ]))->updateQuality();

    $this->assertEquals(80, $items[0]->quality);
    $this->assertEquals(-1, $items[0]->sell_in,);
  }

  public function test_sulfuras_quality_remain_at_80_and_sell_in_unchanged()
  {
    $items = (new GildedRose([
      new Sulfuras(10, 80)
    ]))->updateQuality();

    $this->assertEquals(80, $items[0]->quality);
    $this->assertEquals(10, $items[0]->sell_in);
  }

  public function test_elixir_before_sell_in_date_updates_normally()
  {
    $items = (new GildedRose([
      new ProductItem('Elixir of the Mongoose', 10, 10)
    ]))->updateQuality();

    $this->assertEquals(9, $items[0]->quality);
    $this->assertEquals(9, $items[0]->sell_in);
  }

  public function test_elixir_on_sell_in_date_quality_degrades_twice_as_fast()
  {
    $items = (new GildedRose([
      new ProductItem('Elixir of the Mongoose', 0, 10)
    ]))->updateQuality();

    $this->assertEquals(8, $items[0]->quality);
    $this->assertEquals(-1, $items[0]->sell_in);
  }

  public function test_elixir_after_sell_in_date_quality_degrades_twice_as_fast()
  {
    $items = (new GildedRose([
      new ProductItem('Elixir of the Mongoose', -1, 10)
    ]))->updateQuality();

    $this->assertEquals(8, $items[0]->quality);
    $this->assertEquals(-2, $items[0]->sell_in);
  }

  public function test_dexterity_vest_before_sell_in_date_updates_normally()
  {
    $items = (new GildedRose([
      new ProductItem('+5 Dexterity Vest', 10, 10)
    ]))->updateQuality();

    $this->assertEquals(9, $items[0]->quality);
    $this->assertEquals(9, $items[0]->sell_in);
  }

  public function test_dexterity_vest_on_sell_in_date_quality_degrades_twice_as_fast()
  {
    $items = (new GildedRose([
      new ProductItem('+5 Dexterity Vest', 0, 10)
    ]))->updateQuality();

    $this->assertEquals(8, $items[0]->quality);
    $this->assertEquals(-1, $items[0]->sell_in);
  }

  public function test_dexterity_vest_after_sell_in_date_quality_degrades_twice_as_fast()
  {
    $items = (new GildedRose([
      new ProductItem('+5 Dexterity Vest', -1, 10)
    ]))->updateQuality();

    $this->assertEquals(8, $items[0]->quality);
    $this->assertEquals(-2, $items[0]->sell_in);
  }

  public function test_conjured_before_sell_in_date_updates_normally()
  {
      $items = (new GildedRose([
      new Conjured(10, 10)
      ]))->updateQuality();

      $this->assertEquals(8, $items[0]->quality);
      $this->assertEquals(9, $items[0]->sell_in);
  }

  public function test_conjured_does_not_degrade_passed_zero()
  {
      $items = (new GildedRose([
      new Conjured(10, 0)
      ]))->updateQuality();

      $this->assertEquals(0, $items[0]->quality);
      $this->assertEquals(9, $items[0]->sell_in);
  }

  public function test_conjured_after_sell_in_date_degrades_twice_as_fast()
  {
      $items = (new GildedRose([
      new Conjured(0, 10)
      ]))->updateQuality();

      $this->assertEquals(6, $items[0]->quality);
      $this->assertEquals(-1, $items[0]->sell_in);
  }
}
