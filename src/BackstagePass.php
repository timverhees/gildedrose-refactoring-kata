<?php

  namespace App;

  /*
   * Backstage Pass: "Your pass to fame"
   *
   * The areas of a theatre that are not part of the house or stage are considered part of backstage.
   * These areas include dressing rooms, green rooms, offstage areas (i.e. wings), cross-overs, fly rails or linesets,
   * dimmer rooms, shops and storage areas.
   * https://en.wikipedia.org/wiki/Parts_of_a_theatre#Backstage_or_offstage
   *
  */

  class BackstagePass
  {

    const name = "Backstage passes to a TAFKAL80ETC concert"; # const name. identifies the Product.

    private $max_quality = 50; # max value of the quality. Default: 50
    private $min_quality = 0;  # min value of the quality. Default: 0
    private $quality_step = 1; # the direction and speed of quality change over time.
    private $quality_multipliers = [[10 => 2], [5 => 3]]; # Multiplier if needed Used by default.

    public function __construct($sell_in, $quality)
    {
      if (!is_int($sell_in)) throw new Exception("sell_in integer expected");
      if (!is_int($quality)) throw new Exception("quality integer expected");

      $this->item = new Item(SELF::name, $sell_in, $quality);
      $this->create_item_variables($this->item);
    }

    /*
     * The Item class is a final class, so can't implement it in this class.
     * Copying values from the item class to our class - if new vars are added to the item class, they are
     * automatically added to this class.
     *
     * - All items have a SellIn value which denotes the number of days we have to sell the item
     * - All items have a Quality value which denotes how valuable the item is
     * - At the end of each day our system lowers both values for every item
     */
    private function create_item_variables($item)
    {
      foreach ((array)$item as $variable => $value) {
        $this->{$variable} = $value; # Copying values from the item class to our class
      }
    }

    # Updates Item for the new day.
    public function update()
    {
      $this->sell_in -= 1;

      $this->quality += $this->calculate_quality_step();

      # The Quality of an item is never more than 50
      if ($this->quality >= $this->max_quality)
        $this->quality = $this->max_quality;

      # The Quality of an item is never negative / Quality drops to 0 after the concert
      if ($this->quality < $this->min_quality || $this->sell_in < 0)
        $this->quality = $this->min_quality;
    }

    function calculate_quality_step()
    {
      # "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
      # Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less

      # let's be sure the array is sorted
      ksort($this->quality_multipliers);

      # start with a step
      $step = $this->quality_step;

      # check the multipliers for boundaries and set the right multiplier
      foreach ($this->quality_multipliers as $boundaries) {
        foreach ($boundaries as $boundary => $multiplier) {
          if ($this->sell_in <= $boundary) {
            $step = $this->quality_step * $multiplier;
          }
        }
      }

      return $step;
    }

    public function __toString()
    {
      return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
  }

