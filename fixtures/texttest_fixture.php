<?php

  require_once __DIR__ . '/../vendor/autoload.php';

  use App\GildedRose;
  use App\ProductItem;
  use App\AgedBrie;
  use App\BackstagePass;
  use App\Conjured;
  use App\Sulfuras;

  echo "OMGHAI!\n";

  $items = array(
    new ProductItem("+5 Dexterity Vest", 10, 20),
    new AgedBrie(2, 0),
    new ProductItem("Elixir of the Mongoose", 5, 7),
    new Sulfuras(0, 80),
    new Sulfuras(-1, 80),
    new BackstagePass(15, 20),
    new BackstagePass(10, 49),
    new BackstagePass(5, 49),
    // this conjured item does not work properly yet
    new Conjured(3, 6)
  );

  $app = new GildedRose($items);

  $days = 2;
  if (count($argv) > 1) {
    $days = (int)$argv[1];
  }

  for ($i = 0; $i < $days; $i++) {
    echo("-------- day $i --------\n");
    echo("name, sellIn, quality\n");
    foreach ($items as $item) {
      echo $item . PHP_EOL;
    }
    echo PHP_EOL;
    $app->updateQuality();
  }
