<?php

  namespace App;

  /*
   * Sulfuras: "No kidding around"
   *
   * Sulfuras is the mighty weapon of Ragnaros the Firelord. The weapon never leaves his grasp. It is composed of
   * flaming red elementium and etched from end to end with intricate runes that seem to move like flowing lava across
   * the weapons surface. Sulfuras is covered with many vicious pikes of varying sizes, each white hot and surrounded by
   * the distortion of shimmering heat.
   * https://wow.gamepedia.com/Sulfuras
  */

  class Sulfuras
  {

    const name = "Sulfuras, Hand of Ragnaros"; # const name. identifies the Product.

    public function __construct($sell_in, $quality)
    {
      if (!is_int($sell_in)) throw new Exception("sell_in integer expected");
      if (!is_int($quality)) throw new Exception("quality integer expected");

      $this->item = new Item(SELF::name, $sell_in, $quality);
      $this->create_item_variables($this->item);
    }

    /*
     * The Item class is a final class, so can't implement it in this class.
     * Copying values from the item class to our class - if new vars are added to the item class, they are
     * automatically added to this class.
     *
     * - All items have a SellIn value which denotes the number of days we have to sell the item
     * - All items have a Quality value which denotes how valuable the item is
     * - At the end of each day our system lowers both values for every item
     */
    private function create_item_variables($item)
    {
      foreach ((array)$item as $variable => $value) {
        $this->{$variable} = $value; # Copying values from the item class to our class
      }
    }

    # Updates Item for the new day.
    public function update()
    {
      # "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
      $this->quality = 80;
    }

    public function __toString()
    {
      return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
  }

