<?php

  namespace App;

  /*
   * ProductItem: "Anything you can wish for"
  */

  class ProductItem
  {

    private $max_quality = 50; # max value of the quality. Default: 50
    private $min_quality = 0;  # min value of the quality. Default: 0
    private $quality_step = 1; # the direction and speed of quality change over time.

    public function __construct($name, $sell_in, $quality)
    {
      if (!is_int($sell_in)) throw new Exception("sell_in integer expected");
      if (!is_int($quality)) throw new Exception("quality integer expected");

      $this->item = new Item($name, $sell_in, $quality);
      $this->create_item_variables($this->item);
    }

    /*
     * The Item class is a final class, so can't implement it in this class.
     * Copying values from the item class to our class - if new vars are added to the item class, they are
     * automatically added to this class.
     *
     * - All items have a SellIn value which denotes the number of days we have to sell the item
     * - All items have a Quality value which denotes how valuable the item is
     * - At the end of each day our system lowers both values for every item
     */
    private function create_item_variables($item)
    {
      foreach ((array)$item as $variable => $value) {
        $this->{$variable} = $value; # Copying values from the item class to our class
      }
    }

    # Updates Item for the new day.
    public function update()
    {
      $this->sell_in -= 1;
      $this->quality -= $this->calculate_quality_step();

      # The Quality of an item is never more than 50
      if ($this->quality >= $this->max_quality)
        $this->quality = $this->max_quality;

      # The Quality of an item is never negative
      if ($this->quality < $this->min_quality)
        $this->quality = $this->min_quality;
    }

    private function calculate_quality_step()
    {
      # Once the sell by date has passed, Quality degrades twice as fast
      if ($this->sell_in < 0) {
        return $this->quality_step * 2;
      } else {
        return $this->quality_step;
      }
    }

    public function __toString()
    {
      return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
  }

