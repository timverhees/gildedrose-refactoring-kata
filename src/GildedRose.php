<?php

  namespace App;

  final class GildedRose
  {

    private $items = [];

    public function __construct($items)
    {
      $this->items = $items;
    }

    public function updateQuality()
    {
      foreach ($this->items as $item) {
        try {
          $item->update();
        } catch (Exception $e) {
          throw new Exception ("We have an item quality update problem : {$e->getMessage()}");
        }
      }

      return $this->items;
    }
  }

